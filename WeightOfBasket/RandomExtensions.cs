﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WeightOfBasket
{
    public static class RandomExtensions
    {
        public static int RandomElement(this IEnumerable<int> enumerable)
        {
            return enumerable.RandomElementUsing(new Random());
        }

        public static int RandomElementUsing(this IEnumerable<int> enumerable, Random rand)
        {
            int index = rand.Next(0, enumerable.Count());
            return enumerable.ElementAt(index);
        }
    }
}
