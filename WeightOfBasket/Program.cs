﻿using System;
using System.Collections.Generic;
using WeightOfBasket.Players;

namespace WeightOfBasket
{
    class Program
    {
        static void Main(string[] args)
        {
            int basketWeight = new Random().Next(41, 140);
            int amount = ConsoleGetInt(2, 8, "Please print amount of participating players (should be between 2 and 8): ");

            PrintPlayersInfo();

            List<Player> players = new List<Player>();
            for (int i = 0; i < amount; i++)
            {
                Console.Write("Please Enter players {0} name: ", i + 1);
                string name = Console.ReadLine();
                int playerId = ConsoleGetInt(1, 5, string.Format("Please select player {0} type: ", i + 1));
                Player player = SelectPlayer(playerId, name);
                players.Add(player);
            }

            Console.WriteLine("");
            Console.WriteLine("Lets play the game!!!");
            Console.WriteLine("");
            PlayRoom room = new PlayRoom(41, 140, basketWeight, players);
            room.Play();
            Console.ReadKey();
        }

        private static void PrintPlayersInfo()
        {
            Console.WriteLine("Please choose several from the following players(you have to put their numbers, eg: 1):");
            Console.WriteLine("1. Random Player");
            Console.WriteLine("2. Memory Player");
            Console.WriteLine("3. Thorough Player");
            Console.WriteLine("4. Cheater Player");
            Console.WriteLine("5. Thorough Cheater Player");
        }

        private static Player SelectPlayer(int playerId, string name)
        {
            Player player = null;
            switch ((PlayerType)playerId)
            {
                case PlayerType.RandomPlayer: player = new RandomPlayer(name); break;
                case PlayerType.MemoryPlayer: player = new MemoryPlayer(name); break;
                case PlayerType.ThoroughPlayer: player = new ThoroughPlayer(name); break;
                case PlayerType.CheaterPlayer: player = new CheaterPlayer(name); break;
                case PlayerType.ThoroughCheaterPlayer: player = new ThoroughCheaterPlayer(name); break;
            }
            return player;
        }

        private static int ConsoleGetInt(int rangeStart, int rangeEnd, string message)
        {
            int value = 0;
            while (value < rangeStart || value > rangeEnd)
            {
                Console.Write(message);
                ConsoleKeyInfo key = Console.ReadKey();
                int.TryParse(key.KeyChar.ToString(), out value);
                Console.WriteLine();
            }
            return value;
        }
    }


}
