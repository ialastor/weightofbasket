﻿using System;

namespace WeightOfBasket.Players
{
    public class RandomPlayer : Player
    {
        private Random random;

        public RandomPlayer(string name) : base(name) { }

        public override void Init()
        {
            base.Init();
            random = new Random();
        }

        protected override int RunAlgorithm()
        {
            int value = random.Next(PlayRoom.MinWeight, PlayRoom.MaxWeight);
            return value;
        }

        
    }
}
