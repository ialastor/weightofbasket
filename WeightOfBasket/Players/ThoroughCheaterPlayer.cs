﻿using System.Collections.Generic;
using System.Linq;

namespace WeightOfBasket.Players
{
    public class ThoroughCheaterPlayer : CheaterPlayer
    {
        public ThoroughCheaterPlayer(string name) : base(name) { }

        public override void Init()
        {
            base.Init();
        }
        protected override int RunAlgorithm()
        {
            UseCheatData();
            if (!AllAvailableWeights.Any()) return PlayRoom.GameOverCode;//game over
            int value = AllAvailableWeights.First();
            if (value > PlayRoom.MaxWeight) return PlayRoom.GameOverCode;//game over
            return value;
        }
    }
}
