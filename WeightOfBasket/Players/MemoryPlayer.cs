﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WeightOfBasket.Players
{
    public class MemoryPlayer : Player
    {
        private IEnumerable<int> allAvailableWeights;
        private Random random;

        public MemoryPlayer(string name) : base(name) { }

        public override void Init()
        {
            base.Init();
            random = new Random();
            allAvailableWeights = Enumerable.Range(PlayRoom.MinWeight, PlayRoom.MaxWeight - PlayRoom.MinWeight + 1);
        }

        protected override int RunAlgorithm()
        {
            allAvailableWeights = allAvailableWeights.Except(BucketTries);
            if (!allAvailableWeights.Any()) return PlayRoom.GameOverCode;//game over
            int value = allAvailableWeights.RandomElementUsing(random);
            return value;
        }
    }
}
