﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WeightOfBasket.Players
{
    public class CheaterPlayer : Player
    {
        protected IEnumerable<int> AllAvailableWeights;
        private Random random;
        protected HashSet<int> StolenValues;

        public CheaterPlayer(string name) : base(name) { }

        public override void Init()
        {
            base.Init();
            random = new Random();
            AllAvailableWeights = Enumerable.Range(PlayRoom.MinWeight, PlayRoom.MaxWeight - PlayRoom.MinWeight + 1);
            StolenValues = new HashSet<int>();
            PlayRoom.Players.ForEach(player => player.StoleValue += value => StolenValues.Add(value));
        }

        protected override int RunAlgorithm()
        {
            UseCheatData();
            if (!AllAvailableWeights.Any()) return PlayRoom.GameOverCode;//game over
            int value = AllAvailableWeights.RandomElementUsing(random);
            return value;
        }

        protected void UseCheatData()
        {
            AllAvailableWeights = AllAvailableWeights.Except(StolenValues).ToList();
        }
    }
}
