﻿using System.Collections.Generic;

namespace WeightOfBasket.Players
{
    public abstract class Player
    {
        #region properties
        public string Name { get; private set; }

        public bool Victory { get; set; }

        public PlayRoom PlayRoom { get; set; }

        public List<int> BucketTries { get; set; }

        public Dictionary<int, int> PlayerSleep { get; set; }

        public int TotalSleepTime { get; set; }

        public delegate void StoleValueDelegate(int value);

        public StoleValueDelegate StoleValue { get; set; }
        #endregion

        public Player(string name)
        {
            Name = name;
        }

        protected abstract int RunAlgorithm();

        protected void PostGuessWeight(int value)
        {
            BucketTries.Add(value);
            StoleValue?.Invoke(value);
        }

        public virtual void Init()
        {
            TotalSleepTime = 0;
            BucketTries = new List<int>();
            PlayerSleep = new Dictionary<int, int>();
        }

        public int GuessWeight()
        {
            int value = RunAlgorithm();
            PostGuessWeight(value);
            return value;
        }
    }
}
