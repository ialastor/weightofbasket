﻿using System.Linq;

namespace WeightOfBasket.Players
{
    public class ThoroughPlayer : Player
    {
        public ThoroughPlayer(string name) : base(name) { }

        protected override int RunAlgorithm()
        {
            int value = BucketTries.Any() ? BucketTries.Last() + 1 : PlayRoom.MinWeight;
            if (value > PlayRoom.MaxWeight) return PlayRoom.GameOverCode;//game over
            return value;
        }
    }
}
