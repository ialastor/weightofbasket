﻿namespace WeightOfBasket
{
    public enum PlayerType
    {
        RandomPlayer = 1,
        MemoryPlayer = 2,
        ThoroughPlayer = 3,
        CheaterPlayer = 4,
        ThoroughCheaterPlayer = 5
    }
}
