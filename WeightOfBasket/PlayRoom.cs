﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WeightOfBasket.Players;

namespace WeightOfBasket
{
    public class PlayRoom
    {
        public int GameOverCode { get; private set; }
        public int MinWeight { get; private set; }
        public int MaxWeight { get; private set; }
        public int BasketWeight { get; private set; }
        public List<Player> Players { get; set; }

        public PlayRoom(int minWeight, int maxWeight, int basketWeight, IEnumerable<Player> players)
        {
            GameOverCode = -1;
            MinWeight = minWeight;
            MaxWeight = maxWeight;
            BasketWeight = basketWeight;
            Players = new List<Player>(players);
            Players.ForEach(x =>
            {
                x.PlayRoom = this;
                x.Init();
            });
        }

        public void Play()
        {
            Console.WriteLine("Weight of the basket is {0}", BasketWeight);

            while (true)
            {
                Player player = Players.OrderBy(x => x.TotalSleepTime).First();
                int currentWeight = player.GuessWeight();
                int sleepTime = Math.Abs(currentWeight - BasketWeight);
                player.TotalSleepTime += sleepTime;

                if (!player.PlayerSleep.ContainsKey(currentWeight))
                    player.PlayerSleep.Add(currentWeight, player.TotalSleepTime);

                if (BasketWeight == currentWeight) player.Victory = true;
                //Console.WriteLine("player {0,-21} tries {1,-3} sleeps {2,-3}, total sleep {3,-3}", player.Name, currentWeight, sleepTime, player.TotalSleepTime);

                //break if: any have won, all spent 1500 ms or all have spent 100 tries
                if (player.Victory || AllSpent1500ms() || AllSpent100Tries()) break;
            }

            AnnounceWinner();
        }

        #region private methods
        private bool AllSpent1500ms()
        {
            return Players.All(x => x.TotalSleepTime > 1500);
        }

        private bool AllSpent100Tries()
        {
            return Players.All(x => x.PlayerSleep.ContainsKey(GameOverCode));
        }

        private void AnnounceWinner()
        {
            var winner = Players.SingleOrDefault(x => x.Victory);
            if (winner == null)
            {
                ConcurrentDictionary<Player, int> candidates = CalculateNearestWeightForAll();
                KeyValuePair<Player, int> first = GetNearestOfAll(candidates);
                Console.WriteLine("Nearest: {0}, his suggestion was {1}", first.Key.Name, first.Value);
            }
            else
            {
                Console.WriteLine("Player {0} won, total amount of attempts {1}", winner.Name, Players.Select(x => x.BucketTries.Count()).Sum());
            }
        }

        /// <summary>
        /// Returns the most nearest player and his suggested weight
        /// </summary>
        /// <param name="players">All players and their most nearest weights</param>
        private KeyValuePair<Player, int> GetNearestOfAll(ConcurrentDictionary<Player, int> players)
        {
            var mostNearest = players.GroupBy(x => Math.Abs(BasketWeight - x.Value))//group by abs
                .OrderBy(x => x.Key).First();//order result selection and get the first one (the most nearest to the bucket)
            //Get the first to appear Player and his suggestion
            KeyValuePair<Player, int> first = mostNearest.OrderBy(x => x.Key.PlayerSleep[x.Value]).First();
            return first;
        }

        /// <summary>
        /// Returns players and their suggested nearest weights
        /// </summary>
        private ConcurrentDictionary<Player, int> CalculateNearestWeightForAll()
        {
            ConcurrentDictionary<Player, int> candidates = new ConcurrentDictionary<Player, int>();
            Parallel.ForEach(Players, player =>
            {
                //Get nearest weight
                int bucketNearest = player.BucketTries.OrderBy(x => x).Aggregate((nearest, next) => GetNearest(nearest, next));
                candidates.TryAdd(player, bucketNearest);
            });
            return candidates;
        }

        private int GetNearest(int nearest, int next)
        {
            if (nearest == 0) nearest = next;
            nearest = Math.Abs(BasketWeight - nearest) > Math.Abs(BasketWeight - next) ? next : nearest;
            return nearest;
        }
        #endregion
    }
}
